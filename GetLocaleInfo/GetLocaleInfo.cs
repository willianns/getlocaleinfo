﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Net;
using System.Text;
using Microsoft.SqlServer.Server;
using System.IO;
using System.Xml;

public partial class UserDefinedFunctions
{
    private static readonly string ServiceUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=";

    /// <summary>
    /// Retorna em formato xml os dados do endereço informado
    /// Exemplo:
    ///   http://maps.googleapis.com/maps/api/geocode/xml?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=true
    /// </summary>
    /// <param name="address"></param>
    /// <returns>SqlXml</returns>
    [SqlFunction]
    public static SqlXml GetLocaleInfo(SqlString address)
    {
        var request = WebRequest.Create(String.Format("{0}{1}{2}", ServiceUrl, address.ToString(), "&sensor=true")) as HttpWebRequest;
        string result = null;

        using (var response = request.GetResponse() as HttpWebResponse)
        {
            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(String.Format("Server error (HTTP {0}: {1}).",
                                    response.StatusCode,
                                    response.StatusDescription));

            var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);

            result = reader.ReadToEnd();
        }

        return new SqlXml(XmlReader.Create(new StringReader(result)));
    }
};

