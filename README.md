Get geocode from google maps api with a SQL/CLR function

Usage requirements:
  1- SQL Server with enabled CLR

  --command to enable clr
  sp_configure 'clr enabled', 1;
  GO
  RECONFIGURE;
  GO

  2- Database with enabled TRUSTWORTHY 

  --command to enable TRUSTWORTHY 
  ALTER DATABASE myDbName SET TRUSTWORTHY ON
  GO